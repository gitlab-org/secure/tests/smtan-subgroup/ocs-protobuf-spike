package main

import (
	"bufio"
	"bytes"
	"compress/gzip"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"os"
	"proto_ocs/prototool"
	"time"

	"google.golang.org/protobuf/proto"
	"k8s.io/cli-runtime/pkg/genericclioptions"
	"k8s.io/client-go/kubernetes"
	"k8s.io/kubectl/pkg/cmd/util"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func main() {
	payload := prototool.Payload{}
	payload.Vulnerabilities = make([]*prototool.Vulnerability, 21000)

	for i := 0; i < len(payload.Vulnerabilities); i++ {

		kubernetesResource := &prototool.KubernetesResource{
			Namespace: fmt.Sprintf("%s", randomString(10)),
			Kind: []string{
				"pod", "replicaset", "replicationcontroller", "statefulset", "daemonset", "cronjob", "job",
			}[rand.Intn(7)],
			Name:          fmt.Sprintf("app-%d", rand.Intn(100)),
			ContainerName: fmt.Sprintf("app-%d", rand.Intn(10)),
		}

		location := &prototool.Location{
			KubernetesResource: kubernetesResource,
		}

		payload.Vulnerabilities[i] = &prototool.Vulnerability{
			Name:        fmt.Sprintf("CVE-%d-%d in libc", rand.Intn(1000), rand.Intn(10000)),
			Message:     randomString(150),
			Description: randomString(150),
			Solution:    randomString(150),
			Severity:    []string{"low", "medium", "high"}[rand.Intn(3)],
			Confidence:  []string{"unknown", "reasonable", "high"}[rand.Intn(3)],
			Location:    location,
			Identifiers: generateIdentifiers(),
			Links:       generateLinks(),
		}
	}

	// Save vulns to file | json format
	b, err := json.Marshal(&payload)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = os.WriteFile("random_payload.json", b, 0644)
	if err != nil {
		fmt.Println(err)
		return
	}
	printSize("random_payload.json")

	// Save vulnerabilities in protobuf format
	f, err := os.Create("random_payload.pb")
	if err != nil {
		panic(err)
	}

	data, err := proto.Marshal(&payload)
	if err != nil {
		panic(err)
	}

	if _, err := f.Write(data); err != nil {
		panic(err)
	}

	if err := f.Close(); err != nil {
		panic(err)
	}
	printSize("random_payload.pb")

	compressAndEncodePBData()
	uncompressAndDecodePBData()
	compareOriginalAndReversePBData()

	// Function that splits pb.gzip.base64 into configmaps
	// include labels in the configmaps. Also one with a checksum
	kubeClient, err := getKubeClient()
	if err != nil {
		panic(err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(time.Second*60))
	defer cancel()
	createChainedConfigMaps(ctx, kubeClient)
	time.Sleep(10 * time.Second)
	allData := readChainedConfigmaps(ctx, kubeClient)
	fmt.Printf("Length of chained configmap data pb+gzip+base64 = %v KB\n", len(*allData)/1000)
	verifyConfigData(allData)
	deleteConfigmaps(ctx, kubeClient)

}

func compressAndEncodePBData() {
	// Gzip data
	pbdatabytes, err := os.ReadFile("random_payload.pb")
	if err != nil {
		panic(err)
	}

	var buf bytes.Buffer
	gzipWriter, err := gzip.NewWriterLevel(&buf, gzip.BestCompression)
	if err != nil {
		panic(err)
	}

	_, err = gzipWriter.Write(pbdatabytes)
	if err != nil {
		panic(err)
	}

	if nil != gzipWriter.Close() {
		panic(err)
	}

	err = os.WriteFile("random_payload.pb.gzip", buf.Bytes(), 0644)
	if err != nil {
		panic(err)
	}

	printSize("random_payload.pb.gzip")

	// base64 data
	base64f, _ := os.Open("random_payload.pb.gzip")
	content, _ := io.ReadAll(bufio.NewReader(base64f))
	encoded := base64.StdEncoding.EncodeToString(content)
	if err := os.WriteFile("random_payload.pb.gzip.base64", []byte(encoded), 0644); err != nil {
		panic(err)
	}

	printSize("random_payload.pb.gzip.base64")
}

func uncompressAndDecodePBData() {
	// decode base64
	b64DataBytes, err := os.ReadFile("random_payload.pb.gzip.base64")
	if err != nil {
		panic(err)
	}

	decodedDataBytes, err := base64.StdEncoding.DecodeString(string(b64DataBytes))
	if err != nil {
		panic(err)
	}

	err = os.WriteFile("reverse_random_payload.pb.gzip", decodedDataBytes, 0644)
	if err != nil {
		panic(err)
	}

	// unzip data
	gzDataBytes, err := os.ReadFile("reverse_random_payload.pb.gzip")
	if err != nil {
		panic(err)
	}

	gzipReader, err := gzip.NewReader(bytes.NewReader(gzDataBytes))
	if err != nil {
		panic(err)
	}

	decodedPBDataBytes, err := io.ReadAll(gzipReader)
	if err != nil {
		panic(err)
	}

	err = os.WriteFile("reverse_random_payload.pb", decodedPBDataBytes, 0644)
	if err != nil {
		panic(err)
	}
}

func compareOriginalAndReversePBData() {
	// Compare reverse_random_payload.pb and random_payload.pb to ensure they are identical
	PBData, err := os.ReadFile("random_payload.pb")
	if err != nil {
		panic(err)
	}
	PB := &prototool.Payload{}
	err = proto.Unmarshal(PBData, PB)
	if err != nil {
		panic(err)
	}

	reversePBData, err := os.ReadFile("reverse_random_payload.pb")
	if err != nil {
		panic(err)
	}
	reversePB := &prototool.Payload{}
	err = proto.Unmarshal(reversePBData, reversePB)
	if err != nil {
		panic(err)
	}

	if !proto.Equal(PB, reversePB) {
		panic("Payloads are not identical")
	}
	fmt.Println("random_payload.pb and reverse_random_payload.pb are identical")

}

func randomString(n int) string {
	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func generateIdentifiers() []*prototool.Identifier {

	identifiers := []*prototool.Identifier{}
	length := rand.Intn(9) + 1
	stringsLength := rand.Intn(35) + 5
	for i := 0; i < length; i++ {
		identifiers = append(identifiers, &prototool.Identifier{
			Type:  randomString(stringsLength),
			Name:  randomString(stringsLength),
			Value: randomString(stringsLength),
		})
	}
	return identifiers
}

func generateLinks() []*prototool.Link {

	links := []*prototool.Link{}
	length := rand.Intn(9) + 1
	stringsLength := rand.Intn(35) + 5
	for i := 0; i < length; i++ {
		links = append(links, &prototool.Link{
			Name: randomString(stringsLength),
			Url:  randomString(50),
		})
	}
	return links
}

func printSize(name string) {
	fileInfo, err := os.Stat(name)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%v: \t\t\t%vKB \n", name, fileInfo.Size()/1000)
}

func getKubeClient() (kubernetes.Interface, error) {
	kubeConfigFlags := genericclioptions.NewConfigFlags(true)
	k8sFactory := util.NewFactory(kubeConfigFlags)
	kubeClient, err := k8sFactory.KubernetesClientSet()
	if err != nil {
		fmt.Printf("Failed to create a kube client %w", err)
		return nil, err
	}
	return kubeClient, nil
}

func createChainedConfigMaps(ctx context.Context, client kubernetes.Interface) {
	// read the payload that we need to split
	b64DataBytes, err := os.ReadFile("random_payload.pb.gzip.base64")
	if err != nil {
		panic(err)
	}
	fmt.Printf("We need to split %v KB\n", len(b64DataBytes)/1000)

	maxConfigSizeBytes := 900000 // 900KB so a bit less than 1 MB

	i := 0
	configmapSequence := 0
	configmapsSpecs := []*corev1.ConfigMap{}
	for {
		var bytes []byte
		if i >= len(b64DataBytes)-1 {
			// we are done exit
			break
		} else if i < len(b64DataBytes)-1 && (i+maxConfigSizeBytes) > len(b64DataBytes)-1 {
			// fetch the remaining bytes
			bytes = b64DataBytes[i:]
			i = len(b64DataBytes)
		} else {
			// fetch maxConfigSizeBytes
			bytes = b64DataBytes[i:(i + maxConfigSizeBytes)]
			i += maxConfigSizeBytes

		}
		// write the configmap
		configmapSequence += 1
		configmapsSpecs = append(configmapsSpecs, getConfigMapSpecs(bytes, configmapSequence))
	}
	// Remove the last configmap ocs_next label
	cm := configmapsSpecs[len(configmapsSpecs)-1]
	delete(cm.ObjectMeta.Labels, "agent.gitlab.com/ocs-next")
	configmapsSpecs[len(configmapsSpecs)-1] = cm

	// deploy configmaps
	for _, cm := range configmapsSpecs {
		if err := deployConfigmap(ctx, client, cm); err != nil {
			panic(err)
		}
	}
}

func getConfigMapSpecs(bytes []byte, seq int) *corev1.ConfigMap {
	name := fmt.Sprintf("ocs-default-123-%d", seq)
	labels := map[string]string{
		"agent.gitlab.com/scan":     "ocs",
		"agent.gitlab.com/ocs-ns":   "default",
		"agent.gitlab.com/ocs-next": fmt.Sprintf("ocs-default-123-%d", seq+1),
	}
	return &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: "gitlab-agent",
			Labels:    labels,
		},
		BinaryData: map[string][]byte{
			"data": bytes,
		},
	}

}

func deployConfigmap(ctx context.Context, client kubernetes.Interface, cm *corev1.ConfigMap) error {
	_, err := client.CoreV1().ConfigMaps("gitlab-agent").Create(ctx, cm, metav1.CreateOptions{})
	return err
}

func readChainedConfigmaps(ctx context.Context, client kubernetes.Interface) *[]byte {
	// we know the first configmap name
	cmName := "ocs-default-123-1"
	allData := []byte{}
	for {
		fmt.Printf("Reading configmap %v\n", cmName)
		cm, err := client.CoreV1().ConfigMaps("gitlab-agent").Get(ctx, cmName, metav1.GetOptions{})
		if err != nil {
			panic(err)
		}
		bytes, ok := cm.BinaryData["data"]
		if !ok {
			panic("Configmap did not contain data")
		}
		allData = append(allData, bytes...)
		cmName, ok = cm.ObjectMeta.Labels["agent.gitlab.com/ocs-next"]
		if !ok {
			// we are done
			break
		}
	}
	return &allData
}

func verifyConfigData(data *[]byte) {
	gzDataBytes, err := base64.StdEncoding.DecodeString(string(*data))
	if err != nil {
		panic(err)
	}

	gzipReader, err := gzip.NewReader(bytes.NewReader(gzDataBytes))
	if err != nil {
		panic(err)
	}

	reversePBData, err := io.ReadAll(gzipReader)
	if err != nil {
		panic(err)
	}

	////
	PBData, err := os.ReadFile("random_payload.pb")
	if err != nil {
		panic(err)
	}
	PB := &prototool.Payload{}
	err = proto.Unmarshal(PBData, PB)
	if err != nil {
		panic(err)
	}

	reversePB := &prototool.Payload{}
	err = proto.Unmarshal(reversePBData, reversePB)
	if err != nil {
		panic(err)
	}

	if !proto.Equal(PB, reversePB) {
		panic("Payloads are not identical")
	}
	fmt.Println("random_payload.pb and configmap data are identical")

}

func deleteConfigmaps(ctx context.Context, client kubernetes.Interface) {
	list, err := client.CoreV1().ConfigMaps("gitlab-agent").List(ctx, metav1.ListOptions{LabelSelector: "agent.gitlab.com/ocs-ns=default"})
	if err != nil {
		panic(err)
	}
	for _, cm := range list.Items {
		if err := client.CoreV1().ConfigMaps("gitlab-agent").Delete(ctx, cm.GetObjectMeta().GetName(), metav1.DeleteOptions{}); err != nil {
			fmt.Printf("failed to delete %v\n", err.Error())
		}
	}
}
